**Índice**  

 [[_TOC_]]  
  

# Entidad: Entidad: OffStreetParking

[Basado en la entidad Fiware OffStreetParking](https://github.com/smart-data-models/dataModel.Parking/tree/master/OffStreetParking)

Descripción global: **Un sitio, zona de espacio, fuera de la calle, destinado al estacionamiento de vehículos. Por ejemplo un descampado o un parking subterráneo**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: NGSI Tipo de entidad  

##### Atributos descriptivos de la entidad
- `name`: El nombre de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon. 
- `address`: La dirección postal
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen.
- `allowedVehicleType`: Tipo(s) de vehículo permitido(s). El primer elemento de esta matriz DEBE ser el tipo de vehículo principal permitido. Los números de plaza libre de otros tipos de vehículos permitidos pueden notificarse bajo el atributo extraSpotNumber y a través de entidades específicas de tipo ParkingGroup. Los siguientes valores definidos por VehicleTypeEnum, DATEX 2 versión 2.3. Enum:'agriculturalVehicle, anyVehicle, bicycle, bus, caravan, carWithCaravan, carWithTrailer, constructionOrMaintenanceVehicle, lorry, moped, motorcycle, motorcycleWithSideCar, motorscooter, tanker, trailer, van'
- `category`: Categoría(s) del aparcamiento. El propósito de este campo es permitir etiquetar, en términos generales, las entidades de estacionamiento fuera de la calle
- `chargeType`: Tipo(s) de carga realizado(s) por el aparcamiento. Valores permitidos: Algunos de los definidos por la enumeración DATEX II versión 2.3 _ ChargeTypeEnum_. Enum:'additionalIntervalPrice, annualPayment, firstIntervalPrice, flat, free, minimum, maximum, monthlyPayment, other, seasonTicket, temporaryPrice'. O cualquier otra aplicación específica
- `occupancyDetectionType`: Método(s) de detección de la ocupación. Valores permitidos: Los siguientes de DATEX II versión 2.3 OccupancyDetectionTypeEnum. Enum:'balanceo, manual, modelBased, none, singleSpaceDetection'. O cualquier otro específico de la aplicación
- `requiredPermit`: Este atributo captura qué permiso(s) puede(n) ser necesario(s) para aparcar en este sitio. La semántica es que al menos _uno de estos permisos es necesario para aparcar. Cuando un permiso está compuesto por más de un elemento (y) se pueden combinar con un ','. Por ejemplo, "permiso de residente, permiso de discapacitado" significa que se necesita al mismo tiempo un permiso de residente y un permiso de discapacitado para aparcar. Si la lista está vacía, no se necesita ningún permiso. Valores permitidos: Los siguientes, definidos por la enumeración PermitTypeEnum de DATEX II versión 2.3. Enum:'employeePermit, fairPermit, governmentPermit, noPermitNeed, residentPermit, specificIdentifiedVehiclePermit, studentPermit, visitorPermit'. O cualquier otra aplicación específica
- `totalSpotNumber`: El número total de plazas que ofrece este aparcamiento. Este número puede ser difícil de obtener para aquellos aparcamientos en los que las plazas no están claramente marcadas con líneas. Valores permitidos: Cualquier número entero positivo o 0. Referencias normativas: Atributo parkingNumberOfSpaces de la clase ParkingRecord de DATEX 2 versión 2.3.

##### Atributos descriptivos de la entidad (OPCIONALES)
- Ninguno

##### Atributos de la medición
- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT.
- `availableSpotNumber`: El número de plazas disponibles (incluyendo todos los tipos de vehículos o las plazas reservadas, como las de discapacitados, las de larga duración, etc.). Esto puede ser más difícil de estimar en aquellos aparcamientos en los que los límites de las plazas no están claramente marcados por líneas. Valores permitidos: Un número entero positivo, incluyendo el 0. Debe ser menor o igual que totalSpotNumber.

##### Atributos para el GIS / Representar gráficamente la entidad
- `idAparcamiento`: Identificador del aparcamiento que nos permite relacionarlo con la tabla de aparcamiento del esquema gis.
- `availableSpotPercentage`: Expresa el porcentaje de ocupación del aparcamiento. (availableSpotNumber / totalSpotNumber) * 100.

##### Atributos para la monitorización
- `operationalStatus`: Posibles valores: ok, noData
- `inactive`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.
- `owner`: Responsable de los datos de la entidad.
- `ownerEmail`: Email del responsable de los datos de la entidad.
- `respTecnico`: Responsable Tecnico de la integración del parking (Persona de contacto si no se reciben datos).
- `respTecnicoEmail`: Email del responsable Tecnico de la integración del parking.
- `respOCI`: Responsable del proyecto de la OCI.
- `respOCIEmail`: Email del responsable del proyecto de la OCI.
- `respOCITecnico`: Responsable Tecnico de la OCI
- `respOCITecnicoEmail`: Email del responsable tecnico de la OCI.

##### Atributos innecesarios en futuras entidades
- Ninguno


## Lista de entidades que implementan este modelo

| Subservicio    | ID Entidad             |
|----------------|------------------------|
| /trafico       | parking_8              |
| /trafico       | parking_75             |
| /trafico       | parking_120            |
| ....           | ....

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/parking_8' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "2309",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/trafico",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "640e8298-3efe-11ed-a6cc-0a580a820242; cbnotif=4",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"parking_8\",\"type\":\"OffStreetParking\",\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T07:23:03.962Z\",\"metadata\":{}},\"address\":{\"type\":\"Text\",\"value\":\"Maestro Gozalbo\",\"metadata\":{}},\"allowedVehicleType\":{\"type\":\"ArrayText\",\"value\":[\"car\",\"motorcycle\",\"van\"],\"metadata\":{}},\"availableSpotNumber\":{\"type\":\"Number\",\"value\":175,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T07:23:03.962Z\"}}},\"availableSpotPercentage\":{\"type\":\"Number\",\"value\":91.145833333,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T07:23:03.962Z\"}}},\"category\":{\"type\":\"ArrayText\",\"value\":[\"private\",\"underground\",\"barrierAccess\"],\"metadata\":{}},\"chargeType\":{\"type\":\"ArrayText\",\"value\":[\"unknown\"],\"metadata\":{}},\"idAparcamiento\":{\"type\":\"Number\",\"value\":8,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T07:23:03.962Z\"}}},\"location\":{\"type\":\"geo:point\",\"value\":\"39.4643928,-0.3688761\",\"metadata\":{}},\"name\":{\"type\":\"Text\",\"value\":\"REINO DE VALENCIA \",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T07:23:03.962Z\"}}},\"occupancyDetectionType\":{\"type\":\"ArrayText\",\"value\":[\"unknown\"],\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T07:23:03.962Z\"}}},\"inactive\":{\"type\":\"boolean\",\"value\":\"false\",\"metadata\":{}},\"owner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"ownerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"requiredPermit\":{\"type\":\"ArrayText\",\"value\":[\"noPermitNeeded\"],\"metadata\":{}},\"respOCI\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCIEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnico\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respOCITecnicoEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"respServicio\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"respServicioEmail\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{}},\"source\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"totalSpotNumber\":{\"type\":\"Number\",\"value\":192,\"metadata\":{\"TimeInstant\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T07:23:03.962Z\"}}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "parking_8",
        "type": "OffStreetParking",
        "TimeInstant": {
          "type": "DateTime",
          "value": "2022-09-28T07:23:03.962Z",
          "metadata": {}
        },
        "address": {
          "type": "Text",
          "value": "Maestro Gozalbo",
          "metadata": {}
        },
        "allowedVehicleType": {
          "type": "ArrayText",
          "value": ["car", "motorcycle", "van"],
          "metadata": {}
        },
        "availableSpotNumber": {
          "type": "Number",
          "value": 175,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-28T07:23:03.962Z"
            }
          }
        },
        "availableSpotPercentage": {
          "type": "Number",
          "value": 91.145833333,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-28T07:23:03.962Z"
            }
          }
        },
        "category": {
          "type": "ArrayText",
          "value": ["private", "underground", "barrierAccess"],
          "metadata": {}
        },
        "chargeType": {
          "type": "ArrayText",
          "value": ["unknown"],
          "metadata": {}
        },
        "idAparcamiento": {
          "type": "Number",
          "value": 8,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-28T07:23:03.962Z"
            }
          }
        },
        "location": {
          "type": "geo:point",
          "value": "39.4643928,-0.3688761",
          "metadata": {}
        },
        "name": {
          "type": "Text",
          "value": "REINO DE VALENCIA ",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-28T07:23:03.962Z"
            }
          }
        },
        "occupancyDetectionType": {
          "type": "ArrayText",
          "value": ["unknown"],
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-28T07:23:03.962Z"
            }
          }
        },
        "inactive": {
          "type": "boolean",
          "value": "false",
          "metadata": {}
        },
        "owner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "ownerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "requiredPermit": {
          "type": "ArrayText",
          "value": ["noPermitNeeded"],
          "metadata": {}
        },
        "respOCI": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCIEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnico": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respOCITecnicoEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "respServicio": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "respServicioEmail": {
          "type": "Text",
          "value": "",
          "metadata": {}
        },
        "source": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "totalSpotNumber": {
          "type": "Number",
          "value": 192,
          "metadata": {
            "TimeInstant": {
              "type": "DateTime",
              "value": "2022-09-28T07:23:03.962Z"
            }
          }
        }
      }
    ]
  }
}
</details>

Atributos que van al GIS para su representación en una capa
===========================
- El GIS se actualiza mediante una ETL que accede al Postgres.
