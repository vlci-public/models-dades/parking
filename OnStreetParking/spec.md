**Índice**  

 [[_TOC_]]  
  

# Entidad: OnStreetParking

[Basado en la entidad Fiware OnStreetParking](https://github.com/smart-data-models/dataModel.Parking/tree/master/OnStreetParking)

Descripción global: **Un sitio, zona de espacio abierto, en la calle, (con contador o no) con acceso directo desde una carretera, destinado al estacionamiento de vehículos**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad
- `type`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `name`: El nombre de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `address`: Dirección en la que se encuentra el aparcamiento.
- `allowedVehicleType`: Tipo(s) de vehículo permitido(s). El primer elemento de esta matriz DEBE ser el tipo de vehículo principal permitido. Los números de plaza libre de otros tipos de vehículos permitidos pueden notificarse bajo el atributo extraSpotNumber y a través de entidades específicas de tipo ParkingGroup. Los siguientes valores definidos por VehicleTypeEnum, DATEX 2 versión 2.3. Enum:'agriculturalVehicle, anyVehicle, bicycle, bus, caravan, carWithCaravan, carWithTrailer, constructionOrMaintenanceVehicle, lorry, moped, motorcycle, motorcycleWithSideCar, motorscooter, tanker, trailer, van'
- `category`: Categoría(s) del aparcamiento. El propósito de este campo es permitir etiquetar, en términos generales, las entidades de estacionamiento
- `chargeType`: Tipo(s) de carga realizado(s) por el aparcamiento. Valores permitidos: Algunos de los definidos por la enumeración DATEX II versión 2.3 _ ChargeTypeEnum_. Enum:'additionalIntervalPrice, annualPayment, firstIntervalPrice, flat, free, minimum, maximum, monthlyPayment, other, seasonTicket, temporaryPrice'. O cualquier otra aplicación específica
- `occupancyDetectionType`: Método(s) de detección de la ocupación. Valores permitidos: Los siguientes de DATEX II versión 2.3 OccupancyDetectionTypeEnum. Enum:'balanceo, manual, modelBased, none, singleSpaceDetection'. O cualquier otro específico de la aplicación
- `requiredPermit`: Este atributo captura qué permiso(s) puede(n) ser necesario(s) para aparcar en este sitio. La semántica es que al menos \_uno de estos permisos es necesario para aparcar. Cuando un permiso está compuesto por más de un elemento (y) se pueden combinar con un ','. Por ejemplo, "permiso de residente, permiso de discapacitado" significa que se necesita al mismo tiempo un permiso de residente y un permiso de discapacitado para aparcar. Si la lista está vacía, no se necesita ningún permiso. Valores permitidos: Los siguientes, definidos por la enumeración PermitTypeEnum de DATEX II versión 2.3. Enum:'employeePermit, fairPermit, governmentPermit, noPermitNeed, residentPermit, specificIdentifiedVehiclePermit, studentPermit, visitorPermit'. O cualquier otra aplicación específica
  parkingNumberOfSpaces de la clase ParkingRecord de DATEX 2 versión 2.3.
- `project`: Proyecto al que pertenece esta entidad.
- `dataProvider`: Nombre del operador del sistema establecido en la configuración del servicio
- `description`: Descripción de la zona
- `occupancyDetectionType`: Vector de cadenas con un único elemento “singleDetection” (los valores posibles de cada cadena son: balancing, manual, modelBased, none, singleSpaceDetection)
- `refParkingSpots`: Vector de identificadores de los ParkingSpots de la zona [ref]
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen o la URL del objeto de origen
- `totalSpotNumber`: El número total de plazas que ofrece este aparcamiento. Este número puede ser difícil de obtener para aquellos aparcamientos en los que las plazas no están claramente marcadas con líneas. Valores permitidos: Cualquier número entero positivo o 0. Referencias normativas: Atributo

##### Atributos descriptivos de la entidad (OPCIONALES)

- Ninguno

##### Atributos de la medición

- `TimeInstant`: La fecha y la hora del envío del dato del agente IoT.
- `availableSpotNumber`: El número de plazas disponibles (incluyendo todos los tipos de vehículos o las plazas reservadas, como las de discapacitados, las de larga duración, etc.). Esto puede ser más difícil de estimar en aquellos aparcamientos en los que los límites de las plazas no están claramente marcados por líneas. Valores permitidos: Un número entero positivo, incluyendo el 0. Debe ser menor o igual que totalSpotNumber.
- `observationDateTime`: Instante de última observación (ISO 8601 con hora UTC, ejemplo:)
- `occupancyModified`: Instante de última modificación de la información de ocupación (ISO 8601)
- `occupiedSpotNumber`: Número de plazas ocupadas en el instante de observación

##### Atributos para la monitorización

- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `operationalStatus`: Posibles valores: ok, noData.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

##### Atributos innecesarios en futuras entidades

- Ninguno

## Lista de entidades que implementan este modelo

| Subservicio                 | ID Entidad               |
| --------------------------- | ------------------------ |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PVE_04001 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PVE_08001 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PVE_06004 |
| ....                        | ....                     |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/Impulso_ES_VLC_PVE_06004' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /recarga_vehiculo_electrico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```
