**Índice**  

 [[_TOC_]]  
  

# Entidad: ParkingGroup

[Basado en la entidad Fiware ParkingGroup](https://github.com/smart-data-models/dataModel.Parking/tree/master/ParkingGroup)

Descripción global: **Grupo de aparcamientos o entidades ParkingSpot**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad.
- `type`: Tipo de entidad NGSI. Tiene que ser ParkingSpot.

##### Atributos descriptivos de la entidad
- `description`: Una descripción de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `name`: El nombre de este artículo.
- `project`: Proyecto al que pertenece esta entidad.

##### Atributos descriptivos de la entidad (OPCIONALES)
- `allowedVehicleType`:Tipo de vehículo permitido (un grupo de aparcamiento sólo permite un tipo de vehículo). Enum:'bicicleta, autobús, coche, caravana, motocicleta, moto, camión'.
- `chargeType`:Tipo de tarifa(s) aplicada(s) por el aparcamiento. Enum:'precioIntervaloAdicional, pagoAnual, precioPrimerIntervalo, tarifa plana, gratuito, mínimo, máximo, pagoMensual, abonoTemporada, tarifaTemporal, precioTemporal, desconocido, otro'.- `occupancyDetectionType`: Valores permitidos: Los siguientes de DATEX II versión 2.3 OccupancyDetectionTypeEnum. Enum:'balance, manual, modelBased, none, singleSpaceDetection'. O cualquier otro específico de la aplicación.
- `permitActiveHours`:Este atributo permite capturar situaciones en las que un permiso sólo es necesario a determinadas horas o días de la semana. Es un valor estructurado que debe contener una subpropiedad por cada permiso requerido, indicando cuándo está activo el permiso. Si no se especifica nada para un permiso, significará que siempre se necesita un permiso. Un objeto vacío significa que siempre está activo. La sintaxis debe ser conforme con schema.org especificación de horarios de apertura. Por ejemplo, una zona azul que sólo esté activa durante la semana se codificará como 'blueZonePermit': 'Mo,Tu,We,Th,Fr,Sa 09:00-20:00'. Las aplicaciones DEBERÍAN inspeccionar el valor de esta propiedad a nivel de padre si no está definida.
- `requiredPermit`:Este atributo indica qué permiso(s) puede(n) ser necesario(s) para aparcar en este lugar. La semántica es que se necesita al menos uno de estos permisos para aparcar. Cuando un permiso está compuesto por más de un elemento (y) pueden combinarse con un ','. Por ejemplo, 'permisoresidente,permisodiscapacitados' significa que para aparcar se necesita al mismo tiempo un permiso de residente y un permiso de discapacitado. Si la lista está vacía, no se necesita ningún permiso.

##### Atributos de relación con otras entidades (Necesarios si existen esas relaciones)
- `refParkingSite`: Aparcamiento al que pertenece esta zona. Un grupo no puede ser huérfano. Un grupo no puede tener subgrupos. Referencia a un OffStreetParking o a un OnStreetParking.

##### Atributos de la medición
- `totalSpotNumber`: El número total de plazas pertenecientes a este grupo. Valores permitidos: Cualquier número entero positivo o 0.

##### Atributos de la medición (OPCIONALES)
- `occupiedSpotNumber`:Número de plazas ocupadas en el grupo.
- `identifiedVehicles`:Número de vehículos identificados correctamente.

## Atributos especificos de proyectos

##### Impulso C7 - PMR

- `ParkingSpotChangedValue`:
- `ParkingSpotsChanged`:
- `polygon`:Atributo del tipo geox:json que representa el grupo de parkings en un poligono.
- `fraud_status`: Indica si hay alguna incidencia en el grupo. Este atributo será modificado por el sistema gestor del PMR (App + backend). Puede tomar uno de los siguientes valores: "null": no hay alertas. "user_declaration". Actualmente la incidencia configurada es que haya mas plazas ocupadas que identificadas, lo cual se indica con el valor “user_declaration”


## Lista de entidades que implementan este modelo (586 entidades)

| Subservicio    | ID Entidad                   |
|----------------|------------------------------|
| /trafico       | parkingGroup-1634808638196   |
| /trafico       | parkingGroup-1634808638608   |
| /trafico       | ...                          |
| /trafico       | ...                          |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/parkingGroup-1634808638196' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /Trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```
##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)
<details><summary>Click para ver la respuesta</summary>
PENDIENTE
</details>
