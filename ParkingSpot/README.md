Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/parking/-/blob/main/ParkingSpot/spec.md) que se tiene. Contiene las entidades del proyecto Vehículo Eléctrico.

Otras especificaciones históricas
===
Entidades del proyecto C7 de Impulso y del proyecto del Cabanyal (EDUSI) [Especificación C7 + Edusi](https://gitlab.com/vlci-public/models-dades/parking/-/blob/main/ParkingSpot/spec-c7.md)
