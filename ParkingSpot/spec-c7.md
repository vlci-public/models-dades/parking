**Índice**  

 [[_TOC_]]  
  

# Entidad: ParkingSpot

[Basado en la entidad Fiware ParkingSpot](https://github.com/smart-data-models/dataModel.Parking/tree/master/ParkingSpot)

Descripción global: **Una plaza de aparcamiento es una zona bien delimitada donde se puede estacionar un vehículo. Esta entidad es específica del componente C7 de Impulso y del proyecto EDUSI (plazas parking cabayal).**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad.
- `type`: Tipo de entidad NGSI. Tiene que ser ParkingSpot.

##### Atributos descriptivos de la entidad

- `category`: Categoría de la plaza de aparcamiento. En este campo es donde se indica si la plaza es pmr, cyd o taxi.
- `description`: Una descripción de este artículo.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `name`: El nombre de este artículo.
- `project`: Proyecto al que pertenece esta entidad.
- `serialNumber`:El número de serie asignado por el fabricante del dispositivo físico relacionado con el parking. Antes solo estaba asociado al proyecto de EDUSI.

##### Atributos descriptivos de la entidad (OPCIONALES)

- `refDevice`: El dispositivo que representa el sensor físico utilizado para supervisar esta plaza de aparcamiento.
- `refParkingGroup` : Grupo al que pertenece la plaza de aparcamiento. Para simplificar el modelo, sólo se permite un grupo por plaza de aparcamiento.
- `refParkingSite`: Sitio de estacionamiento al que pertenece la plaza.

##### Atributos de la medición

- `dateCreated`: Marca de tiempo de creación de la entidad. Suele ser asignada por la plataforma de almacenamiento.
- `dateModified`: Marca de tiempo de la última modificación de la entidad. Normalmente será asignada por la plataforma de almacenamiento.
- `status`: Estado de la plaza de aparcamiento desde el punto de vista de la ocupación. Enum: 'closed, free, occupied, occupiedWrong, unknown'. Si el valor es “occupiedWrong” es que está ocupada con alguna incidencia, cuyo motivo se indica en el atributo fraudStatus (actualmente si esto pasa es porque se ha superado el tiempo máximo permitido de ocupación de la plaza)
- `fraud_status`: Indica el motivo por lo que el atributo status está en “occupiedWrong”. Si el valor es “time_limit” es porque se ha superado el tiempo máximo permitido de ocupación en la plaza (actualmente es el único motivo).
- `occupancyModified`: Fecha y hora en la que se modifica el atributo status.
- `batteryLevel`: Nivel de batería del dispositivo. Los valores se expresan en mV (por ejemplo un valor 2800 equivale a 2,8V).
- `temp`: Temperatura del dispositivo.

##### Atributos para realizar cálculos / lógica de negocio

- `calculatedTimestamp`: Fecha que indica el momento en el que todos los atributos han sido calculados.Se utiliza como condicion de las suscripciones.

##### Atributos para la monitorización

- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `operationalStatus`: Posibles valores: ok, noData.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

##### Atributos obsoletos

- `respOCI`: Responsable de la OCI.
- `respOCIEmail`: Email del responsable de la OCI.
- `respOCITecnico`: Responsable técnico de la OCI.
- `respOCITecnicoEmail`: Email del responsable técnico de la OCI.
- `respServicio`: Responsable del servicio.
- `respServicioEmail`: Email del responsable del servicio.


## Lista de entidades que implementan este modelo (4100 entidades)

| Subservicio | ID Entidad           |
| ----------- | -------------------- |
| /trafico    | parkingspot-urb55518 |
| /trafico    | parkingspot-urb55519 |
| /trafico    | parkingspot-urb55520 |
| /trafico    | ...                  |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/parkingspot-urb55518' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /trafico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "2183",
    "user-agent": "orion/3.7.0 libcurl/7.74.0",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/trafico",
    "x-auth-token": "XXXXX",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "e9df342b-149c-4e87-943c-0c63b99dd10e; cbnotif=3",
    "ngsiv2-attrsformat": "normalized"
  },
  "bodyRaw": "{\"subscriptionId\":\"XXXXX\",\"data\":[{\"id\":\"parkingspot-urb56903\",\"type\":\"ParkingSpot\",\"category\":{\"type\":\"Text\",\"value\":\"taxi\",\"metadata\":{}},\"dateModified\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T12:20:12.00Z\",\"metadata\":{}},\"description\":{\"type\":\"TextUnrestricted\",\"value\":\"Calle de las blanquerias 5\",\"metadata\":{}},\"fraud_status\":{\"type\":\"Text\",\"value\":\"\",\"metadata\":{\"timestamp\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T12:20:23.000Z\"}}},\"occupancyModified\":{\"type\":\"DateTime\",\"value\":\"2021-06-11T13:37:07.000Z\"},\"batteryLevel\":{\"type\":\"Number\",\"value\":\"100\"},\"temp\":{\"type\":\"Number\",\"value\":\"35\"},\"location\":{\"type\":\"geo:json\",\"value\":{\"type\":\"Point\",\"coordinates\":[-0.3766919,39.4795964]},\"metadata\":{}},\"maintenanceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"maintenanceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"name\":{\"type\":\"TextUnrestricted\",\"value\":\"3021-2\",\"metadata\":{}},\"operationalStatus\":{\"type\":\"Text\",\"value\":\"ok\",\"metadata\":{\"timestamp\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T12:23:27.000Z\"}}},\"project\":{\"type\":\"TextUnrestricted\",\"value\":\"XXXXX\",\"metadata\":{}},\"serialNumber\":{\"type\":\"Text\",\"value\":\"XXXXX\"},\"refDevice\":{\"type\":\"Text\",\"value\":\"866416042602137\",\"metadata\":{\"timestamp\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T12:23:27.000Z\"}}},\"refParkingGroup\":{\"type\":\"Relationship\",\"value\":\"unlinked\",\"metadata\":{}},\"refParkingSite\":{\"type\":\"Relationship\",\"value\":\"onstreetparking-1625078719421\",\"metadata\":{}},\"serviceOwner\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"serviceOwnerEmail\":{\"type\":\"Text\",\"value\":\"XXXXX\",\"metadata\":{}},\"status\":{\"type\":\"Text\",\"value\":\"occupied\",\"metadata\":{\"timestamp\":{\"type\":\"DateTime\",\"value\":\"2022-09-28T12:23:27.000Z\"}}}}]}",
  "body": {
    "subscriptionId": "XXXXX",
    "data": [
      {
        "id": "parkingspot-urb56903",
        "type": "ParkingSpot",
        "category": {
          "type": "Text",
          "value": "taxi",
          "metadata": {}
        },
        "dateModified": {
          "type": "DateTime",
          "value": "2022-09-28T12:20:12.00Z",
          "metadata": {}
        },
        "description": {
          "type": "TextUnrestricted",
          "value": "Calle de las blanquerias 5",
          "metadata": {}
        },
        "fraud_status": {
          "type": "Text",
          "value": "",
          "metadata": {
            "timestamp": {
              "type": "DateTime",
              "value": "2022-09-28T12:20:23.000Z"
            }
          }
        },
        "occupancyModified": {
          "type": "DateTime",
          "value": "2021-06-11T13:37:07.000Z",
          "metadata": {}
        },
        "batteryLevel": {
          "type": "Number",
          "value": 100,
          "metadata": {}
        },
        "temp": {
          "type": "Number",
          "value": 35,
          "metadata": {}
        },
        "location": {
          "type": "geo:json",
          "value": {
            "type": "Point",
            "coordinates": [-0.3766919, 39.4795964]
          },
          "metadata": {}
        },
        "maintenanceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "maintenanceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "name": {
          "type": "TextUnrestricted",
          "value": "3021-2",
          "metadata": {}
        },
        "operationalStatus": {
          "type": "Text",
          "value": "ok",
          "metadata": {
            "timestamp": {
              "type": "DateTime",
              "value": "2022-09-28T12:23:27.000Z"
            }
          }
        },
        "project": {
          "type": "TextUnrestricted",
          "value": "XXXXX",
          "metadata": {}
        },
        "serialNumber": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "refDevice": {
          "type": "Text",
          "value": "866416042602137",
          "metadata": {
            "timestamp": {
              "type": "DateTime",
              "value": "2022-09-28T12:23:27.000Z"
            }
          }
        },
        "refParkingGroup": {
          "type": "Relationship",
          "value": "unlinked",
          "metadata": {}
        },
        "refParkingSite": {
          "type": "Relationship",
          "value": "onstreetparking-1625078719421",
          "metadata": {}
        },
        "serviceOwner": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "serviceOwnerEmail": {
          "type": "Text",
          "value": "XXXXX",
          "metadata": {}
        },
        "status": {
          "type": "Text",
          "value": "occupied",
          "metadata": {
            "timestamp": {
              "type": "DateTime",
              "value": "2022-09-28T12:23:27.000Z"
            }
          }
        }
      }
    ]
  }
}
</details>

# Atributos que van al GIS para su representación en una capa

- El GIS se actualiza mediante una ETL que accede al Postgres.
