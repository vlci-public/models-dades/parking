**Índice**  

 [[_TOC_]]  
  

# Entidad: ParkingSpot

[Basado en la entidad Fiware ParkingSpot](https://github.com/smart-data-models/dataModel.Parking/tree/master/ParkingSpot)

Descripción global: **Una plaza de aparcamiento es una zona bien delimitada donde se puede estacionar un vehículo.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id`: Identificador único de la entidad.
- `type`: Tipo de entidad NGSI. Tiene que ser ParkingSpot.

##### Atributos descriptivos de la entidad

* `category`: Categoría de la plaza de aparcamiento. Posibles valores y su significado:
    * `onStreet`: La plaza de aparcamiento está ubicada en la calle y no tiene un uso específico.
    * `offStreet`: La plaza de aparcamiento está ubicada en una zona de aparcamiento fuera de una calle; por ejemplo un descampado o un aparcamiento subterráneo.
    * `pmr`: La plaza de aparcamiento está ubicada en la calle y tiene un uso restringido como plaza de movilidad reducida.
    * `cyd`: La plaza de aparcamiento está ubicada en la calle y tiene un uso restringido como plaza de carga y descarga.
    * `taxi`: La plaza de aparcamiento está ubicada en la calle y tiene un uso restringido como parada para taxis.
* `description`: Una descripción de este artículo.
* `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
* `address`: Dirección en la que se encuentra el aparcamiento.
* `name`: El nombre de este artículo.
* `project`: Proyecto al que pertenece esta entidad.

##### Atributos de relación con otras entidades (Necesarios si existen esas relaciones)

- `refDevice`: El dispositivo que representa el sensor físico utilizado para supervisar esta plaza de aparcamiento.
- `refParkingGroup` : Grupo al que pertenece la plaza de aparcamiento. Para simplificar el modelo, sólo se permite un grupo por plaza de aparcamiento.
- `refParkingSite`: Sitio de estacionamiento al que pertenece la plaza.

##### Atributos de la medición

- `status`: Estado de la plaza de aparcamiento desde el punto de vista de la ocupación. Enum: 'closed, free, occupied, occupiedWrong, unknown'. Si el valor es “occupiedWrong” es que está ocupada con alguna incidencia, cuyo motivo se indica en el atributo fraudStatus.

##### Atributos de la medición OPCIONALES, específicos de proyecto

- `previouslyChanged`: Instante previo de cambio de de la información de estado de la plaza. Usado por el proyecto de Impulso-Vehículo Eléctrico exclusivamente.
Este campo lo manda el propio detector (DVA), por lo que, los detectores que no sean DVA no lo tendrán. El campo significa “instante del cambio anterior”. El software del proyecto Impulso-Vehículo-Eléctrico no lo usa para nada. Es un campo que manda directamente el equipo y no se puede quitar.
- `fraudStatus`: Indica el motivo por lo que el atributo status está en “occupiedWrong”. Valores posibles son "Texto Descriptivo" o "". Un texto vacío indica que no hay ningún uso indebido. Un texto (cualquiera) indica que existe un uso indebido de la plaza de aparcamiento. Posibles valores de ejemplo: "time_limit" (porque se ha superado el tiempo máximo permitido de ocupación de la plaza). Actualmente no se usa en ningún proyecto activo.

##### Atributos para la monitorización

- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `operationalStatus`: Posibles valores: ok, noData.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.

## Lista de entidades que implementan este modelo (1098 entidades)

| Subservicio                 | ID Entidad                  |
| --------------------------- | --------------------------- |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PSVE_04001_1 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PSVE_04001_2 |
| /recarga_vehiculo_electrico | Impulso_ES_VLC_PSVE_15001_1 |
| /recarga_vehiculo_electrico | ...                         |

##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/Impulso_ES_VLC_PSVE_15001_1' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /recarga_vehiculo_electrico' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

# Atributos que van al GIS para su representación en una capa

- id, location, address, project, refParkingSite, status, operationalStatus, dateObserved